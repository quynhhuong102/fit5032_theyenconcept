﻿-- Creating table 'Booking'
CREATE TABLE [dbo].[Booking] (
	[Id] int IDENTITY(1,1) NOT NULL ,
	[BookingDate] date NOT NULL,
	[LocationLong] decimal(18,10) NOT NULL,
	[LocationLat] decimal(18,10) NOT NULL,
	[TotalCharge] decimal(10,2) NOT NULL,
	[UserId] NVARCHAR (128) NOT NULL 
	PRIMARY KEY (Id)
	FOREIGN KEY (UserId) REFERENCES AspNetUsers(Id)
);
GO


-- Creating table 'News'
CREATE TABLE [dbo].[News] (
	[Id] int IDENTITY(1,1) NOT NULL ,
	[NewsTitle] nvarchar(max) NOT NULL,
	[NewsContent] nvarchar(max) NOT NULL,
	[NewsDate] date NOT NULL,
	PRIMARY KEY (Id)
);
GO