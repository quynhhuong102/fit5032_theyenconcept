namespace FIT5032_TheYENConcept_ver5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class News
    {
        public int Id { get; set; }

        [Required]
        public string NewsTitle { get; set; }

        [Required]
        public string NewsContent { get; set; }

        [Column(TypeName = "date")]
        public DateTime NewsDate { get; set; }
    }
}
