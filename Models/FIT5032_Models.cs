namespace FIT5032_TheYENConcept_ver5.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FIT5032_Models : DbContext
    {
        public FIT5032_Models()
            : base("name=FIT5032_Models")
        {
        }

        public virtual DbSet<AspNetUserRole> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<News> News { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserRoles)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.Bookings)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Booking>()
                .Property(e => e.LocationLong)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Booking>()
                .Property(e => e.LocationLat)
                .HasPrecision(18, 10);

            modelBuilder.Entity<Booking>()
                .Property(e => e.TotalCharge)
                .HasPrecision(10, 2);
        }

        public System.Data.Entity.DbSet<FIT5032_TheYENConcept_ver5.Models.Location> Locations { get; set; }
    }
}
