namespace FIT5032_TheYENConcept_ver5.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Booking")]
    public partial class Booking
    {
        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime BookingDate { get; set; }

        public decimal LocationLong { get; set; }

        public decimal LocationLat { get; set; }

        public decimal TotalCharge { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
}
